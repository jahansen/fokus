#!/bin/bash

gradle clean build
cp "./build/libs/"* ./release/fokus.jar
cp ./release/* ~/.fokus/
