# Fokus Changelog

## Version 0.5.0 (Alpha) - 22nd July, 2018
### New Features
* Added an option for displaying waiting tasks in the list view.

### Bugs
* Lists of tasks and priorities will now scroll as expected.

## Version 0.4.0 (Alpha) - 17th June, 2018
### New Features
* You can now view and edit all fields on a task.

### Improvements
* Moved creating new tasks from a separate drawer to a modal dialog that can be accessed from the Focus and List 
drawers.

## Version 0.3.1 (Alpha) - 4th June, 2018
### Improvements
* Improved formatting on Priorities tab

### Bugs
* Fixed an issue where the create task form prevented entering numbers containing '0' for recur intervals.
* All recur fields will now be cleared upon creating a task.
* Recurring tasks that repeat on certain days will now repeat on the correct day. 

## Version 0.3.0 (Alpha)
### New Features
* You can now order tasks via label using priorities.

## Version 0.2.0 (Alpha)
### New Features
* Tasks can now be made recurring. When a recurring task is completed, it will be recreated after the specified 
interval.

### Improvements
* Added a program icon.
* Made the 'Focus' tab the default.
* Added icons for buttons and tabs.
* Minor layout tweaks.

### Bugs
* You can no longer confirm the snooze dialog without selecting a date.

## Version 0.1.0 (Alpha)
Initial alpha release. Includes basic TODO list and task snoozing functionality. 