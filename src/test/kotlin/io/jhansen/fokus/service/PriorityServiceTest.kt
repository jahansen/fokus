package io.jhansen.fokus.service

import io.jhansen.fokus.model.domain.Priority
import io.jhansen.fokus.store.PriorityStore
import io.kotlintest.Description
import io.kotlintest.specs.ShouldSpec
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.verify

class PriorityServiceTest : ShouldSpec() {

    private lateinit var priorityStore: PriorityStore
    private lateinit var priorityService: PriorityService

    private val priority1 = Priority(1, "P1", 0)
    private val priority2 = Priority(2, "P2", 1)
    private val priority3 = Priority(3, "P3", 2)
    private val priority4 = Priority(4, "P4", 3)
    private val orderedPriorities = listOf(priority1, priority2, priority3, priority4)

    override fun beforeTest(description: Description) {
        priorityStore = mockk()
        priorityService = PriorityService(priorityStore)
    }

    init {
        "PriorityService.reorderPriorityUp" {
            should("do nothing if priority doesn't exist") {
                // Arrange
                mockPriorities()

                // Act
                priorityService.reorderPriorityUp(99L)

                // Assert
                verify(exactly = 0) { priorityStore.updatePriority(priorityId = any(), rank = any()) }
            }

            should("do nothing if priority rank is first in list") {
                // Arrange
                mockPriorities()

                // Act
                priorityService.reorderPriorityUp(orderedPriorities.first().id!!)

                // Assert
                verify(exactly = 0) { priorityStore.updatePriority(priorityId = any(), rank = any()) }
            }

            should("decrement priority's rank by 1") {
                // Arrange
                mockPriorities()
                every { priorityStore.updatePriority(priorityId = any(), rank = any()) } just Runs

                // Act
                priorityService.reorderPriorityUp(priority3.id!!)

                // Assert
                verify(exactly = 1) {
                    priorityStore.updatePriority(priorityId = priority3.id!!, rank = priority3.rank - 1)
                }
            }

            should("increment swapped priority's rank by 1") {
                // Arrange
                mockPriorities()
                every { priorityStore.updatePriority(priorityId = any(), rank = any()) } just Runs

                // Act
                priorityService.reorderPriorityUp(priority3.id!!)

                // Assert
                verify(exactly = 1) {
                    priorityStore.updatePriority(priorityId = priority2.id!!, rank = priority2.rank + 1)
                }
            }
        }

        "PriorityService.reorderPriorityDown" {
            should("do nothing if priority doesn't exist") {
                // Arrange
                mockPriorities()

                // Act
                priorityService.reorderPriorityDown(99L)

                // Assert
                verify(exactly = 0) { priorityStore.updatePriority(priorityId = any(), rank = any()) }
            }

            should("do nothing if priority rank is last in the list") {
                // Arrange
                mockPriorities()

                // Act
                priorityService.reorderPriorityDown(orderedPriorities.last().id!!)

                // Assert
                verify(exactly = 0) { priorityStore.updatePriority(priorityId = any(), rank = any()) }
            }

            should("increment priority's rank by 1") {
                // Arrange
                mockPriorities()
                every { priorityStore.updatePriority(priorityId = any(), rank = any()) } just Runs

                // Act
                priorityService.reorderPriorityDown(priority2.id!!)

                // Assert
                verify(exactly = 1) {
                    priorityStore.updatePriority(priorityId = priority2.id!!, rank = priority2.rank + 1)
                }
            }

            should("decrement swapped priority's rank by 1") {
                // Arrange
                mockPriorities()
                every { priorityStore.updatePriority(priorityId = any(), rank = any()) } just Runs

                // Act
                priorityService.reorderPriorityDown(priority2.id!!)

                // Assert
                verify(exactly = 1) {
                    priorityStore.updatePriority(priorityId = priority3.id!!, rank = priority3.rank - 1)
                }
            }
        }

        "PriorityService.deletePriority" {
            should("do nothing if priority doesn't exist") {
                // Arrange
                val missingPriorityId = 99L
                every { priorityStore.getPriority(priorityId = missingPriorityId) } returns null

                // Act
                priorityService.deletePriority(missingPriorityId)

                // Assert
                verify(exactly = 0) { priorityStore.deletePriority(priorityId = any()) }
            }

            should("delete the priority if it exists") {
                // Arrange
                every { priorityStore.getPriority(priorityId = priority2.id!!) } returns priority2
                every { priorityStore.deletePriority(priorityId = priority2.id!!) } just Runs
                every { priorityStore.getOrderedPriorities() } returns orderedPriorities.filterNot { it == priority2 }
                every { priorityStore.updatePriority(priorityId = any(), rank = any()) } just Runs

                // Act
                priorityService.deletePriority(priority2.id!!)

                // Assert
                verify(exactly = 1) { priorityStore.deletePriority(priorityId = priority2.id!!) }
            }

            should("shift higher priorities down after deleting a priority") {
                // Arrange
                every { priorityStore.getPriority(priorityId = priority2.id!!) } returns priority2
                every { priorityStore.deletePriority(priorityId = priority2.id!!) } just Runs
                every { priorityStore.getOrderedPriorities() } returns orderedPriorities.filterNot { it == priority2 }
                every { priorityStore.updatePriority(priorityId = any(), rank = any()) } just Runs

                // Act
                priorityService.deletePriority(priority2.id!!)

                // Assert
                verify(exactly = 1) { priorityStore.updatePriority(priority3.id!!, priority3.rank - 1) }
                verify(exactly = 1) { priorityStore.updatePriority(priority4.id!!, priority4.rank - 1) }
            }
        }
    }

    private fun mockPriorities() {
        every { priorityStore.getOrderedPriorities() } returns orderedPriorities
    }
}