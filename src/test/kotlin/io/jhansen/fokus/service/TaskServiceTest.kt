package io.jhansen.fokus.service

import io.jhansen.fokus.model.domain.Priority
import io.jhansen.fokus.model.domain.RecurData
import io.jhansen.fokus.model.domain.RecurPeriod
import io.jhansen.fokus.model.domain.Task
import io.jhansen.fokus.store.TaskStore
import io.jhansen.fokus.util.TimeUtil
import io.kotlintest.Description
import io.kotlintest.matchers.collections.shouldHaveElementAt
import io.kotlintest.matchers.collections.shouldHaveSize
import io.kotlintest.specs.ShouldSpec
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.verify
import java.time.LocalDateTime

class TaskServiceTest : ShouldSpec() {

    private lateinit var taskStore: TaskStore
    private lateinit var priorityService: PriorityService
    private lateinit var timeUtil: TimeUtil
    private lateinit var taskService: TaskService

    private val nullWaitUntil = LocalDateTime.of(2000, 1, 1, 0, 0)

    override fun beforeTest(description: Description) {
        taskStore = mockk()
        priorityService = mockk()
        timeUtil = mockk()

        todayIs(LocalDateTime.of(2018, 1, 1, 15, 0)) // 3pm, Wednesday Jan 1st 2018

        taskService = TaskService(taskStore, priorityService, timeUtil)
    }

    init {
        "TaskService.completeTask" {
            should("recur daily task at start of next day") {
                // Arrange
                val task = recurringTask(RecurData(period = RecurPeriod.DAILY, interval = 1))
                mockExistingTask(task)

                // Act
                taskService.completeTask(task.id!!)

                // Assert
                verify(exactly = 1) {
                    taskStore.insertRecurredTask(waitingTask(task, LocalDateTime.of(2018, 1, 2, 0, 0)))
                }
            }

            should("recur n daily task at start of nth day") {
                // Arrange
                val task = recurringTask(RecurData(period = RecurPeriod.DAILY, interval = 4))
                mockExistingTask(task)

                // Act
                taskService.completeTask(task.id!!)

                // Assert
                verify(exactly = 1) {
                    taskStore.insertRecurredTask(waitingTask(task, LocalDateTime.of(2018, 1, 5, 0, 0)))
                }
            }

            should("recur weekly task at start of selected day when one day is selected") {
                // Arrange
                val task = recurringTask(RecurData(period = RecurPeriod.WEEKLY, interval = 1, recurOnFriday = true))
                mockExistingTask(task)

                // Act
                taskService.completeTask(task.id!!)

                // Assert
                verify(exactly = 1) {
                    taskStore.insertRecurredTask(waitingTask(task, LocalDateTime.of(2018, 1, 5, 0, 0)))
                }
            }

            should("recur weekly task at start of next selected day when multiple days are selected") {
                // Arrange
                val task = recurringTask(RecurData(
                        period = RecurPeriod.WEEKLY, interval = 1, recurOnWednesday = true, recurOnFriday = true)
                )
                mockExistingTask(task)

                // Act
                taskService.completeTask(task.id!!)

                // Assert
                verify(exactly = 1) {
                    taskStore.insertRecurredTask(waitingTask(task, LocalDateTime.of(2018, 1, 3, 0, 0)))
                }
            }

            should("recur weekly task at start of next selected day when current day is between selected days") {
                // Arrange
                todayIs(LocalDateTime.of(2018, 1, 3, 15, 0)) // 3pm, Wednesday Jan 5th 2018
                val task = Task(
                        id = 1L,
                        summary = "Task",
                        waitUntil = nullWaitUntil,
                        recurData = RecurData(
                                id = 1L,
                                period = RecurPeriod.WEEKLY,
                                interval = 1,
                                recurOnMonday = true,
                                recurOnFriday = true
                        )
                )
                mockExistingTask(task)

                // Act
                taskService.completeTask(task.id!!)

                // Assert
                verify(exactly = 1) {
                    taskStore.insertRecurredTask(waitingTask(task, LocalDateTime.of(2018, 1, 5, 0, 0)))
                }
            }

            should("recur n weekly task at start of selected day after n-1 weeks, when triggered before day") {
                // Arrange
                val task = recurringTask(RecurData(period = RecurPeriod.WEEKLY, interval = 3, recurOnThursday = true))
                mockExistingTask(task)

                // Act
                taskService.completeTask(task.id!!)

                // Assert
                verify(exactly = 1) {
                    taskStore.insertRecurredTask(waitingTask(task, LocalDateTime.of(2018, 1, 18, 0, 0)))
                }
            }

            should("recur n weekly task at start of selected day after n weeks, when triggered after day") {
                // Arrange
                todayIs(LocalDateTime.of(2018, 1, 5, 15, 0)) // 3pm, Friday Jan 5th 2018
                val task = Task(
                        id = 1L,
                        summary = "Task",
                        waitUntil = nullWaitUntil,
                        recurData = RecurData(id = 1L, period = RecurPeriod.WEEKLY, interval = 3, recurOnThursday = true)
                )
                mockExistingTask(task)

                // Act
                taskService.completeTask(task.id!!)

                // Assert
                verify(exactly = 1) {
                    taskStore.insertRecurredTask(waitingTask(task, LocalDateTime.of(2018, 1, 25, 0, 0)))
                }
            }

            should("recur weekly task with no selected day 1 week later at start of day") {
                // Arrange
                val task = recurringTask(RecurData(period = RecurPeriod.WEEKLY, interval = 1))
                mockExistingTask(task)

                // Act
                taskService.completeTask(task.id!!)

                // Assert
                verify(exactly = 1) {
                    taskStore.insertRecurredTask(waitingTask(task, LocalDateTime.of(2018, 1, 8, 0, 0)))
                }
            }

            should("recur n weekly task with no selected day n weeks later at start of day") {
                // Arrange
                val task = recurringTask(RecurData(period = RecurPeriod.WEEKLY, interval = 3))
                mockExistingTask(task)

                // Act
                taskService.completeTask(task.id!!)

                // Assert
                verify(exactly = 1) {
                    taskStore.insertRecurredTask(waitingTask(task, LocalDateTime.of(2018, 1, 22, 0, 0)))
                }
            }
        }

        "TaskService.updateTask" {
            should("update an existing task") {
                // Arrange
                val updatedTask = Task(id = 1L, summary = "task", waitUntil = nullWaitUntil)
                mockExistingTask(updatedTask)

                // Act
                taskService.updateTask(updatedTask)

                // Assert
                verify(exactly = 1) {
                    taskStore.updateTask(updatedTask)
                }
            }

            should("delete the recur template if the task no longer recurs") {
                // Arrange
                val updatedTask = Task(id = 1L, summary = "task", waitUntil = nullWaitUntil)
                val persistedTask = recurringTask(RecurData(period = RecurPeriod.DAILY, interval = 1))

                every { taskStore.getTask(updatedTask.id!!) } returns persistedTask
                every { taskStore.updateTask(any()) } just Runs
                every { taskStore.deleteRecurTemplate(persistedTask.recurData?.id!!) } just Runs

                // Act
                taskService.updateTask(updatedTask)

                // Assert
                verify(exactly = 1) {
                    taskStore.deleteRecurTemplate(persistedTask.recurData?.id!!)
                }
            }

            should("do nothing if the given task does not have an id") {
                // Arrange
                val updatedTask = Task(id = null, summary = "task", waitUntil = nullWaitUntil)

                // Act
                taskService.updateTask(updatedTask)

                // Assert
                verify(exactly = 0) {
                    taskStore.updateTask(any())
                }
            }

            should("do nothing if the task does not exist") {
                // Arrange
                val updatedTask = Task(id = 1L, summary = "task", waitUntil = nullWaitUntil)
                every { taskStore.getTask(updatedTask.id!!) } returns null

                // Act
                taskService.updateTask(updatedTask)

                // Assert
                verify(exactly = 0) {
                    taskStore.updateTask(any())
                }
            }
        }

        "TaskService.deleteTask" {
            should("delete recur template if present") {
                // Arrange
                val task = Task(id = 1L, summary = "Task", waitUntil = nullWaitUntil, recurData = RecurData(
                        id = 2L, period = RecurPeriod.DAILY, interval = 1
                ))
                mockExistingTask(task)
                every { taskStore.deleteRecurTemplate(any()) } just Runs
                every { taskStore.deleteTask(any()) } just Runs

                // Act
                taskService.deleteTask(task.id!!)

                // Assert
                verify(exactly = 1) { taskStore.deleteRecurTemplate(task.recurData!!.id!!) }
            }

            should("delete task if present") {
                // Arrange
                val task = Task(id = 1L, summary = "Task", waitUntil = nullWaitUntil)
                mockExistingTask(task)
                every { taskStore.deleteTask(any()) } just Runs

                // Act
                taskService.deleteTask(task.id!!)

                // Assert
                verify(exactly = 1) { taskStore.deleteTask(task.id!!) }
            }

            should("do nothing when task not present") {
                // Arrange
                every { taskStore.getTask(any()) } returns null

                // Act
                taskService.deleteTask(1L)

                // Assert
                verify(exactly = 0) { taskStore.deleteTask(any()) }
            }
        }

        "TaskService.getPrioritisedTasks" {
            should("return tasks in priority order") {
                val alpha1 = Task(id = 1L, summary = "Alpha 1", labels = "alpha", waitUntil = nullWaitUntil)
                val alpha2 = Task(id = 2L, summary = "Alpha 2", labels = "alpha", waitUntil = nullWaitUntil)
                val beta1 = Task(id = 3L, summary = "Beta 1", labels = "beta", waitUntil = nullWaitUntil)
                val gamma1 = Task(id = 4L, summary = "Gamma 1", labels = "gamma", waitUntil = nullWaitUntil)
                val omega1 = Task(id = 5L, summary = "Omega 1", labels = "", waitUntil = nullWaitUntil)
                val omega2 = Task(id = 6L, summary = "Omega 2", labels = "", waitUntil = nullWaitUntil)

                val incompleteTasks = listOf(gamma1, alpha1, omega1, beta1, omega2, alpha2)
                every { taskStore.getIncompleteTasks() } returns incompleteTasks

                val priorityAlpha = Priority(id = 1L, label = "alpha", rank = 0)
                val priorityBeta = Priority(id = 2L, label = "beta", rank = 1)
                val priorityGamma = Priority(id = 3L, label = "gamma", rank = 2)

                val priorities = listOf(priorityAlpha, priorityBeta, priorityGamma)
                every { priorityService.getOrderedPriorities() } returns priorities

                every { taskStore.getTasksForLabel("alpha", false, false) } returns listOf(alpha1, alpha2)
                every { taskStore.getTasksForLabel("beta", false, false) } returns listOf(beta1)
                every { taskStore.getTasksForLabel("gamma", false, false) } returns listOf(gamma1)

                // Act
                val result = taskService.getPrioritisedTasks(false)

                // Assert
                result.shouldHaveElementAt(0, alpha1)
                result.shouldHaveElementAt(1, alpha2)
                result.shouldHaveElementAt(2, beta1)
                result.shouldHaveElementAt(3, gamma1)
                result.shouldHaveSize(incompleteTasks.size)
            }
        }
    }

    private fun recurringTask(recurData: RecurData): Task {
        return Task(
                id = 1L,
                summary = "Task",
                waitUntil = nullWaitUntil,
                recurData = recurData.copy(id = 1L)
        )
    }

    private fun waitingTask(task: Task, waitUntil: LocalDateTime): Task {
        return Task(
                id = task.id,
                summary = task.summary,
                waitUntil = waitUntil,
                recurData = task.recurData
        )
    }

    private fun mockExistingTask(task: Task) {
        every { taskStore.getTask(task.id!!) } returns task
        every { taskStore.insertRecurredTask(any()) } returns task.id
        every { taskStore.completeTask(any()) } just Runs
        every { taskStore.updateTask(any()) } just Runs
    }

    private fun todayIs(dateTime: LocalDateTime) {
        every { timeUtil.now() } returns dateTime
    }
}