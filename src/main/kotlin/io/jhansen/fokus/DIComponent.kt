package io.jhansen.fokus

import dagger.Component
import io.jhansen.fokus.controller.FokusController
import javax.inject.Singleton

/**
 * Fokus uses Dagger 2 for dependency injection, but it's necessary to integrate this with TornadoFX's own dependency
 * injection system. In short, TornadoFX-specific objects like Views have their own way of fetching injected objects,
 * but once we're into code that isn't TornadoFX-specific it's Dagger all the way down.
 *
 * This class represents the root of the graph, and is the access point through which TornadoFX accesses our code. You
 * can add new classes to be injected by annotating them with @Singleton (if they are indeed a singleton) and
 * prefixing their constructor with @Inject. More advanced usages are explained in the user guide:
 * https://google.github.io/dagger/users-guide
 *
 * Note that this is currently simple because we only need one entrypoint into the graph. If we need to add more
 * entrypoints (ie. top-level classes that TornadoFX Views need to access directly), then we may need to do some
 * hackery to get it working properly, because I believe Dagger doesn't let you fetch just anything you want from
 * any position in the graph.
 */
@Singleton
@Component
interface DIComponent {
    fun root(): FokusController
}