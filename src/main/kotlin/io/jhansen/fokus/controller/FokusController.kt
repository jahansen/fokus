package io.jhansen.fokus.controller

import io.jhansen.fokus.model.mutable.MutablePriority
import io.jhansen.fokus.model.domain.Task
import io.jhansen.fokus.model.mutable.MutableTask
import io.jhansen.fokus.service.PriorityService
import io.jhansen.fokus.service.TaskService
import javafx.beans.property.SimpleListProperty
import javafx.collections.FXCollections
import tornadofx.* // ktlint-disable no-wildcard-imports
import java.time.Duration
import java.time.LocalDateTime
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FokusController @Inject constructor(
    private val taskService: TaskService,
    private val priorityService: PriorityService
) {
    val focusedTask = SimpleListProperty<MutableTask>()
    val priorities = SimpleListProperty<MutablePriority>()
    val taskList = SimpleListProperty<MutableTask>()

    var showWaitingTasks = false

    // Must be called after FokusController construction
    fun initFilter(showWaiting: Boolean) {
        showWaitingTasks = showWaiting
        refreshTasks()
        refreshPriorities()
    }

    fun snoozeTask(task: Task, duration: Duration) {
        taskService.snoozeTask(task, duration)
        refreshTasks()
    }

    fun snoozeTask(task: Task, untilDate: LocalDateTime) {
        taskService.snoozeTask(task, untilDate)
        refreshTasks()
    }

    fun createTask(task: Task) {
        taskService.createTask(task)
        refreshTasks()
    }

    fun completeTask(taskId: Long) {
        taskService.completeTask(taskId)
        refreshTasks()
    }

    fun updateTask(task: Task) {
        taskService.updateTask(task)
        refreshTasks()
    }

    fun deleteTask(taskId: Long) {
        taskService.deleteTask(taskId)
        refreshTasks()
    }

    fun createPriority(label: String) {
        priorityService.createPriority(label)
        refreshPriorities()
        refreshTasks()
    }

    fun reorderPriorityUp(priorityId: Long) {
        priorityService.reorderPriorityUp(priorityId)
        refreshPriorities()
        refreshTasks()
    }

    fun reorderPriorityDown(priorityId: Long) {
        priorityService.reorderPriorityDown(priorityId)
        refreshPriorities()
        refreshTasks()
    }

    fun deletePriority(priorityId: Long) {
        priorityService.deletePriority(priorityId)
        refreshPriorities()
        refreshTasks()
    }

    fun updateFilter(showWaiting: Boolean) {
        showWaitingTasks = showWaiting
        refreshTasks()
    }

    private fun refreshTasks() {
        val filteredTasks = taskService.getPrioritisedTasks(showWaitingTasks).map { MutableTask(it) }
        taskList.value = FXCollections.observableArrayList(filteredTasks)

        val orderedTasks = taskService.getPrioritisedTasks(false).map { MutableTask(it) }
        val test = FXCollections.observableArrayList(orderedTasks)
        focusedTask.value = if (orderedTasks.isEmpty()) { listOf<MutableTask>().observable() } else
            listOf(test[0]!!).observable()
    }

    private fun refreshPriorities() {
        val orderedPriorities = priorityService.getOrderedPriorities()
        val mappedPriorities = priorityService.getOrderedPriorities().mapIndexed {
            index, priority ->
            MutablePriority(priority, index == orderedPriorities.size - 1)
        }
        priorities.value = FXCollections.observableArrayList(mappedPriorities)
    }
}