package io.jhansen.fokus.store

import io.jhansen.fokus.database.RecurTemplate
import io.jhansen.fokus.database.Tasks
import io.jhansen.fokus.model.domain.RecurData
import io.jhansen.fokus.model.domain.RecurPeriod
import io.jhansen.fokus.model.domain.Task
import org.jetbrains.exposed.sql.Op
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SqlExpressionBuilder
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import java.time.LocalDateTime
import java.time.ZoneOffset
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TaskStore @Inject constructor() {

    /**
     * Inserts a new task, creating a new recur template if present in the received data.
     */
    fun insertTask(newTask: Task): Long? {
        return transaction {
            val recurId = if (newTask.recurData != null) {
                insertRecurTemplate(newTask.recurData)
            } else {
                null
            }

            val taskInsert = Tasks.insert {
                it[summary] = newTask.summary
                it[labels] = newTask.labels
                it[wait_until] = waitUntilOrDefault(newTask.waitUntil)
                it[completed] = false
                it[recur_id] = recurId
            }

            taskInsert[Tasks.id]
        }
    }

    /**
     * Inserts a new recurring task, assuming that its recur template is already present in the database.
     *
     * TODO: Should this take a RecurringTask object that has non-nullable RecurData?
     */
    fun insertRecurredTask(task: Task): Long? {
        return transaction {
            val taskInsert = Tasks.insert {
                it[summary] = task.summary
                it[labels] = task.labels
                it[wait_until] = waitUntilOrDefault(task.waitUntil)
                it[completed] = false
                it[recur_id] = task.recurData!!.id // TODO: Ideally, in this context RecurData should be non-nullable
            }

            taskInsert[Tasks.id]
        }
    }

    fun getTask(taskId: Long): Task? {
        return transaction {
            selectTasksWhere { Tasks.id eq taskId }.first()
        }
    }

    fun completeTask(taskId: Long) {
        transaction {
            Tasks.update({ Tasks.id eq taskId }) {
                it[completed] = true
            }
        }
    }

    fun updateWaitUntil(taskId: Long, waitUntil: LocalDateTime) {
        transaction {
            Tasks.update({ Tasks.id eq taskId }) {
                it[wait_until] = waitUntil.toEpochSecond(ZoneOffset.UTC)
            }
        }
    }

    fun updateTask(task: Task) {
        transaction {
            val recurId = if (task.recurData != null) {
                val recurData = task.recurData
                if (recurData.id == null) {
                    insertRecurTemplate(recurData)
                } else {
                    RecurTemplate.update({ RecurTemplate.id eq recurData.id }) {
                        it[period] = recurData.period.ordinal
                        it[interval] = recurData.interval
                        it[recurOnMonday] = recurData.recurOnMonday
                        it[recurOnTuesday] = recurData.recurOnTuesday
                        it[recurOnWednesday] = recurData.recurOnWednesday
                        it[recurOnThursday] = recurData.recurOnThursday
                        it[recurOnFriday] = recurData.recurOnFriday
                        it[recurOnSaturday] = recurData.recurOnSaturday
                        it[recurOnSunday] = recurData.recurOnSunday
                    }

                    recurData.id
                }
            } else {
                null
            }

            Tasks.update({ Tasks.id eq task.id!! }) {
                it[summary] = task.summary
                it[labels] = task.labels
                it[wait_until] = waitUntilOrDefault(task.waitUntil)
                it[completed] = task.completed
                it[recur_id] = recurId
            }
        }
    }

    fun deleteTask(taskId: Long) {
        transaction {
            Tasks.deleteWhere { Tasks.id eq taskId }
        }
    }

    fun deleteRecurTemplate(recurTemplateId: Long) {
        transaction {
            // First remove any remaining references to this recur template
            Tasks.update({ Tasks.recur_id eq recurTemplateId }) {
                it[recur_id] = null
            }
            RecurTemplate.deleteWhere { RecurTemplate.id eq recurTemplateId }
        }
    }

    fun getAllTasks(): List<Task> {
        return transaction {
            tasksJoin.selectAll().map { mapToTask(it) }
        }
    }

    fun getIncompleteTasks(includeWaiting: Boolean = false): List<Task> {
        return if (!includeWaiting) {
            transaction {
                selectTasksWhere {
                    Tasks.completed eq false and
                    Tasks.wait_until.lessEq(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC))
                }
            }
        } else {
            transaction {
                selectTasksWhere {
                    Tasks.completed eq false
                }
            }
        }
    }

    fun getTasksForLabel(label: String, includeCompleted: Boolean, includeWaiting: Boolean): List<Task> {
        return if (includeWaiting) {
            transaction {
                selectTasksWhere {
                    Tasks.completed eq includeCompleted and
                    Tasks.labels.like("%$label%")
                }
            }
        } else {
            transaction {
                selectTasksWhere {
                    Tasks.completed eq includeCompleted and
                    Tasks.labels.like("%$label%") and
                    Tasks.wait_until.lessEq(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC))
                }
            }
        }
    }

    private val tasksJoin = Tasks.leftJoin(RecurTemplate)

    private fun selectTasksWhere(where: SqlExpressionBuilder.() -> Op<Boolean>): List<Task> {
        return tasksJoin.select(where).map { mapToTask(it) }
    }

    private fun insertRecurTemplate(recurData: RecurData): Long? {
        val recurInsert = RecurTemplate.insert {
            it[period] = recurData.period.ordinal
            it[interval] = recurData.interval
            it[recurOnMonday] = recurData.recurOnMonday
            it[recurOnTuesday] = recurData.recurOnTuesday
            it[recurOnWednesday] = recurData.recurOnWednesday
            it[recurOnThursday] = recurData.recurOnThursday
            it[recurOnFriday] = recurData.recurOnFriday
            it[recurOnSaturday] = recurData.recurOnSaturday
            it[recurOnSunday] = recurData.recurOnSunday
        }

        return recurInsert[RecurTemplate.id]
    }

    private fun mapToTask(row: ResultRow): Task {
        val recurData = if (row[Tasks.recur_id] != null) {
            RecurData(
                    id = row[RecurTemplate.id],
                    period = RecurPeriod.fromIndex(row[RecurTemplate.period]),
                    interval = row[RecurTemplate.interval],
                    recurOnMonday = row[RecurTemplate.recurOnMonday],
                    recurOnTuesday = row[RecurTemplate.recurOnTuesday],
                    recurOnWednesday = row[RecurTemplate.recurOnWednesday],
                    recurOnThursday = row[RecurTemplate.recurOnThursday],
                    recurOnFriday = row[RecurTemplate.recurOnFriday],
                    recurOnSaturday = row[RecurTemplate.recurOnSaturday],
                    recurOnSunday = row[RecurTemplate.recurOnSunday]
            )
        } else {
            null
        }

        return Task(
                id = row[Tasks.id],
                summary = row[Tasks.summary],
                labels = row[Tasks.labels],
                waitUntil = LocalDateTime.ofEpochSecond(row[Tasks.wait_until], 0, ZoneOffset.UTC),
                completed = row[Tasks.completed],
                recurData = recurData
        )
    }

    // TODO: The wait_until column should be nullable - this function is a hack until that's fixed
    private fun waitUntilOrDefault(waitUntil: LocalDateTime?): Long {
        return waitUntil?.toEpochSecond(ZoneOffset.UTC)
                ?: LocalDateTime.of(2018, 1, 1, 1, 1).toEpochSecond(ZoneOffset.UTC)
    }
}