package io.jhansen.fokus.store

import io.jhansen.fokus.database.Priorities
import io.jhansen.fokus.model.domain.Priority
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PriorityStore @Inject constructor() {

    fun insertPriority(newPriority: Priority): Long? {
        return transaction {
            val priorityInsert = Priorities.insert {
                it[label] = newPriority.label
                it[rank] = newPriority.rank
            }

            priorityInsert[Priorities.id]
        }
    }

    fun getPriority(priorityId: Long): Priority? {
        return transaction {
            Priorities.select { Priorities.id eq priorityId }.map { mapToPriority(it) }.first()
        }
    }

    /**
     * Returns all priorities, ordered by ascending rank.
     */
    fun getOrderedPriorities(): List<Priority> {
        return transaction {
            Priorities.selectAll().orderBy(Priorities.rank).map { mapToPriority(it) }
        }
    }

    fun countPriorities(): Int {
        return transaction {
            Priorities.selectAll().count()
        }
    }

    fun updatePriority(priorityId: Long, rank: Int) {
        return transaction {
            Priorities.update({ Priorities.id eq priorityId }) {
                it[Priorities.rank] = rank
            }
        }
    }

    fun deletePriority(priorityId: Long) {
        transaction {
            Priorities.deleteWhere { Priorities.id eq priorityId }
        }
    }

    private fun mapToPriority(row: ResultRow): Priority {
        return Priority(
                id = row[Priorities.id],
                label = row[Priorities.label],
                rank = row[Priorities.rank]
        )
    }
}