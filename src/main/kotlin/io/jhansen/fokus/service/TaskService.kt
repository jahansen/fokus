package io.jhansen.fokus.service

import io.jhansen.fokus.model.domain.RecurPeriod
import io.jhansen.fokus.model.domain.Task
import io.jhansen.fokus.store.TaskStore
import io.jhansen.fokus.util.TimeUtil
import java.time.DayOfWeek
import java.time.Duration
import java.time.LocalDateTime
import java.time.temporal.TemporalAdjusters.next
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TaskService @Inject constructor(
    private val taskStore: TaskStore,
    private val priorityService: PriorityService,
    private val timeUtil: TimeUtil
) {

    private val daysOfWeek = DayOfWeek.values().toList()

    fun snoozeTask(task: Task, duration: Duration) {
        val updatedWaitUntil = timeUtil.now().plus(duration)
        taskStore.updateWaitUntil(task.id!!, updatedWaitUntil)
    }

    fun snoozeTask(task: Task, waitUntilDate: LocalDateTime) {
        taskStore.updateWaitUntil(task.id!!, waitUntilDate)
    }

    fun createTask(task: Task) {
        taskStore.insertTask(task)
    }

    fun completeTask(taskId: Long) {
        val task = taskStore.getTask(taskId)
        recurTask(task!!)

        taskStore.completeTask(taskId)
    }

    fun updateTask(updatedTask: Task) {
        val existingTask = taskStore.getTask(updatedTask.id ?: return) ?: return

        if (updatedTask.recurData == null && existingTask.recurData != null) {
            taskStore.deleteRecurTemplate(existingTask.recurData.id!!)
        }

        taskStore.updateTask(updatedTask)
    }

    fun deleteTask(taskId: Long) {
        val task = taskStore.getTask(taskId)
        if (task != null) {
            if (task.recurData != null) {
                taskStore.deleteRecurTemplate(task.recurData.id!!) // TODO: Should have a 'persisted' RecurTemplate object
            }
            taskStore.deleteTask(taskId)
        }
    }

    fun getPrioritisedTasks(includeWaiting: Boolean): List<Task> {
        val tasks = taskStore.getIncompleteTasks(includeWaiting)

        val priorityResults = priorityService.getOrderedPriorities().map {
            priority -> taskStore.getTasksForLabel(priority.label, false, includeWaiting)
        }.flatten()

        return listOf(priorityResults, tasks).flatten().distinct()
    }

    private fun recurTask(task: Task) {
        if (task.recurData != null) {
            val recurData = task.recurData
            when (recurData.period) {
                RecurPeriod.DAILY -> {
                    val recurDate = timeUtil.now().plusDays(recurData.interval.toLong()).toLocalDate().atStartOfDay()
                    val recurredTask = task.copy(waitUntil = recurDate)
                    taskStore.insertRecurredTask(recurredTask)
                }
                RecurPeriod.WEEKLY -> {
                    val today = timeUtil.now()
                    val nextRecurDayList = listOf(
                            recurData.recurOnMonday,
                            recurData.recurOnTuesday,
                            recurData.recurOnWednesday,
                            recurData.recurOnThursday,
                            recurData.recurOnFriday,
                            recurData.recurOnSaturday,
                            recurData.recurOnSunday
                    )

                    val currentDayIndex = today.dayOfWeek.ordinal
                    val rearrangedNextRecurDayList = rearrangeDayList(nextRecurDayList, currentDayIndex)
                    val nextRecurDayIndex = rearrangedNextRecurDayList.indexOfFirst { it == true }

                    if (nextRecurDayIndex < 0) {
                        val recurDate = today.plusWeeks(recurData.interval.toLong()).toLocalDate().atStartOfDay()
                        val recurredTask = task.copy(waitUntil = recurDate)
                        taskStore.insertRecurredTask(recurredTask)
                    } else {
                        val rearrangedDayOfWeekList = rearrangeDayList(daysOfWeek, currentDayIndex)
                        val nextRecurDay = rearrangedDayOfWeekList[nextRecurDayIndex]
                        val recurDate = today
                                .plusWeeks((recurData.interval - 1).toLong())
                                .with(next(nextRecurDay)).toLocalDate().atStartOfDay()
                        val recurredTask = task.copy(waitUntil = recurDate)
                        taskStore.insertRecurredTask(recurredTask)
                    }
                }
            }
        }
    }

    private fun <T> rearrangeDayList(list: List<T>, currentDayIndex: Int): List<T> {
        return list.subList(currentDayIndex, list.size) + list.subList(0, currentDayIndex)
    }
}