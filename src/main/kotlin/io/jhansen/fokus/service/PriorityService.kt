package io.jhansen.fokus.service

import io.jhansen.fokus.model.domain.Priority
import io.jhansen.fokus.store.PriorityStore
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PriorityService @Inject constructor(private val priorityStore: PriorityStore) {

    fun createPriority(label: String) {
        val newPriority = Priority(
                label = label,
                rank = priorityStore.countPriorities()
        )
        priorityStore.insertPriority(newPriority)
    }

    /**
     * Returns all priorities, ordered by ascending rank
     */
    fun getOrderedPriorities(): List<Priority> {
        return priorityStore.getOrderedPriorities()
    }

    fun reorderPriorityUp(priorityId: Long) {
        val allPriorities = priorityStore.getOrderedPriorities()
        val priorityToMoveUp = allPriorities.find { priorityId == it.id }

        if (priorityToMoveUp != null) {
            val currentRank = priorityToMoveUp.rank
            if (currentRank <= 0) {
//                log.warning("Cannot move priority with rank 0 up.")
            } else {
                priorityStore.updatePriority(priorityId, currentRank - 1)
                val priorityToMoveDown = allPriorities.find { it.rank == priorityToMoveUp.rank - 1 }
                if (priorityToMoveDown != null) {
                    priorityStore.updatePriority(priorityToMoveDown.id!!, currentRank)
                }
            }
        }
    }

    fun reorderPriorityDown(priorityId: Long) {
        val allPriorities = priorityStore.getOrderedPriorities()
        val priorityToMoveDown = allPriorities.find { priorityId == it.id }

        if (priorityToMoveDown != null) {
            val currentRank = priorityToMoveDown.rank
            if (currentRank >= allPriorities.size - 1) {
//                log.warning("Cannot move final priority down.")
            } else {
                priorityStore.updatePriority(priorityId, currentRank + 1)
                val priorityToMoveUp = allPriorities.find { it.rank == priorityToMoveDown.rank + 1 }
                if (priorityToMoveUp != null) {
                    priorityStore.updatePriority(priorityToMoveUp.id!!, currentRank)
                }
            }
        }
    }

    /**
     * Deletes the given priority and shifts the rank of the remaining priorities up
     */
    fun deletePriority(priorityId: Long) {
        val priorityToDelete = priorityStore.getPriority(priorityId)
        if (priorityToDelete != null) {
            priorityStore.deletePriority(priorityId)

            val prioritiesToShiftUp = priorityStore.getOrderedPriorities().filter { it.rank > priorityToDelete.rank }
            prioritiesToShiftUp.forEach { priorityStore.updatePriority(it.id!!, it.rank - 1) }
        }
    }
}