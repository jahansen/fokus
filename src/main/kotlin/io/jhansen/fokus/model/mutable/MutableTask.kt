package io.jhansen.fokus.model.mutable

import io.jhansen.fokus.model.domain.Task
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import java.time.LocalDate
import java.time.LocalDateTime
import tornadofx.* // ktlint-disable no-wildcard-imports

class MutableTask(task: Task) {
    val idProperty = SimpleObjectProperty<Long>(this, "id", task.id)
    var id by idProperty

    val summaryProperty = SimpleStringProperty(this, "summary", task.summary)
    var summary by summaryProperty

    val labelsProperty = SimpleStringProperty(this, "labels", task.labels)
    var labels by labelsProperty

    val waitUntilProperty = SimpleObjectProperty<LocalDateTime>(this, "waitUntil", task.waitUntil)
    var waitUntil by waitUntilProperty

    // Same as waitUntil LocalDateTime, but in LocalDate form for consumption by the JavaFX datepicker
    val waitUntilDateProperty = SimpleObjectProperty<LocalDate>(this, "waitUntilDate", task.waitUntil?.toLocalDate())

    val isWaitingProperty = SimpleBooleanProperty(this, "isWaiting",
            task.waitUntil?.isAfter(LocalDateTime.now()) ?: false)

    val completedProperty = SimpleObjectProperty<Boolean>(this, "completed", task.completed)
    var completed by completedProperty

    val recurringProperty = SimpleBooleanProperty(this, "recurring", task.recurData?.id != null)
    var recurring by recurringProperty

    val recurData = SimpleObjectProperty<MutableRecurData>(
            this,
            "recurData",
            task.recurData?.let { MutableRecurData(it) } ?: MutableRecurData())
}