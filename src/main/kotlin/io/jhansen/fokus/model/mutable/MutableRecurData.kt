package io.jhansen.fokus.model.mutable

import io.jhansen.fokus.model.domain.RecurData
import io.jhansen.fokus.model.domain.RecurPeriod
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty

class MutableRecurData(recurData: RecurData) {

    constructor() : this(RecurData(interval = 1))

    val idProperty = SimpleObjectProperty<Long>(this, "id", recurData.id)
    val periodProperty = SimpleObjectProperty<RecurPeriod>(this, "recurPeriod", recurData.period)
    val periodStringProperty = SimpleStringProperty(this, "recurPeriodString", recurData.period.label)
    val intervalProperty = SimpleIntegerProperty(this, "interval", recurData.interval)
    val recurOnMondayProperty = SimpleBooleanProperty(this, "recurOnMonday", recurData.recurOnMonday)
    val recurOnTuesdayProperty = SimpleBooleanProperty(this, "recurOnTuesday", recurData.recurOnTuesday)
    val recurOnWednesdayProperty = SimpleBooleanProperty(this, "recurOnWednesday", recurData.recurOnWednesday)
    val recurOnThursdayProperty = SimpleBooleanProperty(this, "recurOnThursday", recurData.recurOnThursday)
    val recurOnFridayProperty = SimpleBooleanProperty(this, "recurOnFriday", recurData.recurOnFriday)
    val recurOnSaturdayProperty = SimpleBooleanProperty(this, "recurOnSaturday", recurData.recurOnSaturday)
    val recurOnSundayProperty = SimpleBooleanProperty(this, "recurOnSunday", recurData.recurOnSunday)
}