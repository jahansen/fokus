package io.jhansen.fokus.model.mutable

import io.jhansen.fokus.model.domain.Priority
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty

class MutablePriority(priority: Priority, isLast: Boolean = false) {
    val idProperty = SimpleObjectProperty<Long>(this, "id", priority.id)
    val labelProperty = SimpleStringProperty(this, "label", priority.label)
    val rankProperty = SimpleIntegerProperty(this, "rank", priority.rank)
    val isLastProperty = SimpleBooleanProperty(this, "isLast", isLast)
}