package io.jhansen.fokus.model.view

import io.jhansen.fokus.model.mutable.MutableTask
import tornadofx.* // ktlint-disable no-wildcard-imports

class EditTaskViewModel(val task: MutableTask) : ViewModel() {
    val summary = bind { task.summaryProperty }
    val labels = bind { task.labelsProperty }
    val waitUntil = bind { task.waitUntilDateProperty }
    val recurring = bind { task.recurringProperty }
    val period = bind { task.recurData.value.periodStringProperty }
    val interval = bind { task.recurData.value.intervalProperty }
    val recurOnMonday = bind { task.recurData.value.recurOnMondayProperty }
    val recurOnTuesday = bind { task.recurData.value.recurOnTuesdayProperty }
    val recurOnWednesday = bind { task.recurData.value.recurOnWednesdayProperty }
    val recurOnThursday = bind { task.recurData.value.recurOnThursdayProperty }
    val recurOnFriday = bind { task.recurData.value.recurOnFridayProperty }
    val recurOnSaturday = bind { task.recurData.value.recurOnSaturdayProperty }
    val recurOnSunday = bind { task.recurData.value.recurOnSundayProperty }
}