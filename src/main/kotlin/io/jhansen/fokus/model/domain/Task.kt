package io.jhansen.fokus.model.domain

import io.jhansen.fokus.view.TaskCardItemViewModel
import java.time.LocalDateTime

data class Task(
    val id: Long? = null,
    val summary: String,
    val labels: String = "",
    val waitUntil: LocalDateTime? = null,
    val completed: Boolean = false,
    val recurData: RecurData? = null
) {
    constructor(taskCardModel: TaskCardItemViewModel): this(
            id = taskCardModel.id.value,
            summary = taskCardModel.summary.value,
            labels = taskCardModel.labels.value,
            waitUntil = taskCardModel.waitUntil.value,
            completed = taskCardModel.completed.value,
            recurData = taskCardModel.recurData.value?.let { RecurData(it) }
    )

    fun isPersisted(): Boolean {
        return id != null
    }
}
