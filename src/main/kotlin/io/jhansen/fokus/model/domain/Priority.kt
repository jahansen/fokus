package io.jhansen.fokus.model.domain

data class Priority(val id: Long? = null, val label: String, val rank: Int)