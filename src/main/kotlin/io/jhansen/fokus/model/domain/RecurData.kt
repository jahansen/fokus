package io.jhansen.fokus.model.domain

import io.jhansen.fokus.model.mutable.MutableRecurData

data class RecurData(
    val id: Long? = null,
    val period: RecurPeriod = RecurPeriod.values().first(),
    val interval: Int,
    val recurOnMonday: Boolean = false,
    val recurOnTuesday: Boolean = false,
    val recurOnWednesday: Boolean = false,
    val recurOnThursday: Boolean = false,
    val recurOnFriday: Boolean = false,
    val recurOnSaturday: Boolean = false,
    val recurOnSunday: Boolean = false
) {
    constructor(recurData: MutableRecurData): this(
            id = recurData.idProperty.value,
            period = recurData.periodProperty.value,
            interval = recurData.intervalProperty.value,
            recurOnMonday = recurData.recurOnMondayProperty.value,
            recurOnTuesday = recurData.recurOnTuesdayProperty.value,
            recurOnWednesday = recurData.recurOnWednesdayProperty.value,
            recurOnThursday = recurData.recurOnThursdayProperty.value,
            recurOnFriday = recurData.recurOnFridayProperty.value,
            recurOnSaturday = recurData.recurOnSaturdayProperty.value,
            recurOnSunday = recurData.recurOnSundayProperty.value
    )
}

enum class RecurPeriod(val label: String) {
    DAILY("days"),
    WEEKLY("weeks");

    companion object {
        fun fromIndex(value: Int): RecurPeriod {
            return RecurPeriod.values().first { it.ordinal == value }
        }
    }
}
