package io.jhansen.fokus.view

import io.jhansen.fokus.controller.FokusController
import io.jhansen.fokus.extensions.datetimepicker
import io.jhansen.fokus.model.domain.Task
import javafx.beans.property.SimpleObjectProperty
import tornadofx.* // ktlint-disable no-wildcard-imports
import java.time.DayOfWeek
import java.time.Duration
import java.time.LocalDateTime
import java.time.temporal.TemporalAdjusters.next

class SnoozeModal : Fragment() {
    private val controller: FokusController by di()
    private val task: Task by param()

    private val snoozeDateProperty = SimpleObjectProperty<LocalDateTime>()

    override val root = vbox {
        hbox {
            button("5min") {
                action {
                    controller.snoozeTask(task, Duration.ofMinutes(5))
                    close()
                }
            }
            button("30min") {
                action {
                    controller.snoozeTask(task, Duration.ofMinutes(30))
                    close()
                }
            }
            button("1h") {
                action {
                    controller.snoozeTask(task, Duration.ofHours(1))
                    close()
                }
            }
        }
        hbox {
            button("1d") {
                action {
                    controller.snoozeTask(task, daysInFuture(1))
                    close()
                }
            }
            button("1w") {
                action {
                    controller.snoozeTask(task, daysInFuture(7))
                    close()
                }
            }
            button("2w") {
                action {
                    controller.snoozeTask(task, daysInFuture(14))
                    close()
                }
            }
            button("4w") {
                action {
                    controller.snoozeTask(task, daysInFuture(28))
                    close()
                }
            }
        }
        hbox {
            button("Sunday") {
                action {
                    val updatedDate = LocalDateTime.now().with(next(DayOfWeek.SUNDAY))
                    controller.snoozeTask(task, updatedDate)
                    close()
                }
            }
        }
        hbox {
            datetimepicker(snoozeDateProperty) {
                format = "eeee, dd MMMM yyyy, h:mm a"
            }
            button("Ok") {
                enableWhen(snoozeDateProperty.isNotNull)
                action {
                    controller.snoozeTask(task, snoozeDateProperty.value)
                    close()
                }
            }
            style {
                maxWidth = 320.px
            }
        }
    }

    private fun daysInFuture(days: Long): LocalDateTime {
        return LocalDateTime.now().plusDays(days).toLocalDate().atStartOfDay()
    }
}