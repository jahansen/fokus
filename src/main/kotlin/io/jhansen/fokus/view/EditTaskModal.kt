package io.jhansen.fokus.view

import io.jhansen.fokus.controller.FokusController
import io.jhansen.fokus.model.view.EditTaskViewModel
import io.jhansen.fokus.model.domain.RecurData
import io.jhansen.fokus.model.domain.RecurPeriod
import io.jhansen.fokus.model.domain.Task
import io.jhansen.fokus.model.mutable.MutableTask
import javafx.scene.control.ComboBox
import javafx.util.converter.NumberStringConverter
import tornadofx.* // ktlint-disable no-wildcard-imports

class EditTaskModal : Fragment() {
    private val controller: FokusController by di()
    private val task: MutableTask by param()
    private val model = EditTaskViewModel(task)

    private var periodField: ComboBox<String> by singleAssign()
    private val recurPeriods = RecurPeriod.values().map { it.label }

    override val root = form {
        fieldset("Update task") {
            field("Summary") {
                textfield(model.summary).validator {
                    if (it.isNullOrBlank()) error("Summary is required") else null
                }
                maxWidth = 1000.0
            }
            field("Labels") {
                textfield(model.labels)
                maxWidth = 1000.0
            }
            field("Wait until") {
                datepicker(model.waitUntil)
            }

            checkbox(text = "Recur", property = model.recurring)
            form {
                visibleWhen(model.recurring)
                vbox {
                    hbox {
                        label("Repeat every ")
                        textfield(model.interval, NumberStringConverter()) {
                            validator {
                                if (model.recurring.value && it.isNullOrBlank()) error("Interval is required") else null
                            }
                            filterInput { it.controlNewText.isInt().and(it.controlNewText != "0") }
                            maxWidth = 50.0
                        }
                        region {
                            minWidth = 10.0
                        }
                        periodField = combobox(property = model.period, values = recurPeriods)
                        paddingBottom = 10
                    }
                    hbox {
                        enableWhen(periodField.valueProperty().isEqualTo("weeks"))
                        label("On: ")
                        checkbox("Mo", model.recurOnMonday) { paddingRight = 10 }
                        checkbox("Tu", model.recurOnTuesday) { paddingRight = 10 }
                        checkbox("We", model.recurOnWednesday) { paddingRight = 10 }
                        checkbox("Th", model.recurOnThursday) { paddingRight = 10 }
                        checkbox("Fr", model.recurOnFriday) { paddingRight = 10 }
                        checkbox("Sa", model.recurOnSaturday) { paddingRight = 10 }
                        checkbox("Su", model.recurOnSunday) { paddingRight = 10 }
                    }
                }
            }
        }

        hbox {
            paddingBottom = 20

            button("Update") {
                enableWhen(model.dirty.and(model.valid))
                action {
                    val recurData = if (model.recurring.value) {
                        val recurPeriod = RecurPeriod.fromIndex(periodField.selectionModel.selectedIndex)
                        val weeklyPeriod = recurPeriod == RecurPeriod.WEEKLY
                        RecurData(
                                id = task.recurData.value.idProperty.value,
                                period = recurPeriod,
                                interval = model.interval.value.toInt(),
                                recurOnMonday = if (weeklyPeriod) model.recurOnMonday.value else false,
                                recurOnTuesday = if (weeklyPeriod) model.recurOnTuesday.value else false,
                                recurOnWednesday = if (weeklyPeriod) model.recurOnWednesday.value else false,
                                recurOnThursday = if (weeklyPeriod) model.recurOnThursday.value else false,
                                recurOnFriday = if (weeklyPeriod) model.recurOnFriday.value else false,
                                recurOnSaturday = if (weeklyPeriod) model.recurOnSaturday.value else false,
                                recurOnSunday = if (weeklyPeriod) model.recurOnSunday.value else false
                        )
                    } else {
                        null
                    }

                    // NOTE: The waituntil field should ideally allow nulls - to be done after DB migrations are
                    // implemented
                    val updatedTask = Task(
                            id = task.id,
                            summary = model.summary.value,
                            labels = model.labels.value,
                            waitUntil = model.waitUntil.value?.atStartOfDay(),
                            completed = false,
                            recurData = recurData
                    )

                    controller.updateTask(updatedTask)

                    close()
                }
            }

            region {
                minWidth = 10.0
            }

            button("Close") {
                action {
                    close()
                }
            }
        }
    }
}