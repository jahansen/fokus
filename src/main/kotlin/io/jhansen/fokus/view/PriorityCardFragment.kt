package io.jhansen.fokus.view

import io.jhansen.fokus.controller.FokusController
import io.jhansen.fokus.model.mutable.MutablePriority
import javafx.beans.property.ObjectProperty
import javafx.scene.layout.Priority
import org.controlsfx.glyphfont.FontAwesome
import org.controlsfx.glyphfont.GlyphFontRegistry
import tornadofx.* // ktlint-disable no-wildcard-imports

class PriorityCardFragment : ListCellFragment<MutablePriority>() {
    private val controller: FokusController by di()
    private val priority = PriorityCardItemViewModel(itemProperty)

    private val fontAwesome = GlyphFontRegistry.font("FontAwesome")
    private val deleteGlyph = fontAwesome.create(FontAwesome.Glyph.TRASH)
    private val upArrowGlyph = fontAwesome.create(FontAwesome.Glyph.ARROW_UP)
    private val downArrowGlyph = fontAwesome.create(FontAwesome.Glyph.ARROW_DOWN)

    override val root = borderpane {
        top = hbox {
            label(priority.label)
            region {
                hboxConstraints {
                    hGrow = Priority.ALWAYS
                }
            }
            button(graphic = upArrowGlyph) {
                action {
                    controller.reorderPriorityUp(priority.id.value)
                }
                disableWhen(priority.rank.isEqualTo(0))
                tooltip("Shift priority up")
            }
            button(graphic = downArrowGlyph) {
                action {
                    controller.reorderPriorityDown(priority.id.value)
                }
                disableWhen(priority.isLast)
                tooltip("Shift priority down")
            }
            button(graphic = deleteGlyph) {
                action {
                    controller.deletePriority(priority.id.value)
                }
                tooltip("Delete priority")
            }
        }
    }
}

class PriorityCardItemViewModel(property: ObjectProperty<MutablePriority>) : ItemViewModel<MutablePriority>(itemProperty = property) {
    val id = bind { item?.idProperty }
    val label = bind { item?.labelProperty }
    // If we don't have the explicit type here, the consumer can't use comparison methods
    val rank: BindingAwareSimpleIntegerProperty = bind { item?.rankProperty }
    val isLast = bind { item?.isLastProperty }
}