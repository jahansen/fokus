package io.jhansen.fokus.view

import io.jhansen.fokus.controller.FokusController
import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.TextField
import tornadofx.* // ktlint-disable no-wildcard-imports

class CreatePriorityModal : Fragment() {
    private val controller: FokusController by di()
    private val labelProperty = SimpleStringProperty()
    private var labelField: TextField by singleAssign()

    override val root =
        form {
            fieldset {
                field("Label") {
                    labelField = textfield() // TODO: For some reason this gets bound with the field label if I use a property here
                }
                button("Ok") {
//                    enableWhen(labelProperty.isNotEmpty)
                    action {
                        // TODO: The button should be disabled instead of doing this check, but I need the property to work
                        if (labelField.text.isNotEmpty()) {
                            controller.createPriority(labelField.text)
                            close()
                        }
                    }
                }
            }
        }
}