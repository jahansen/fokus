package io.jhansen.fokus.view

import io.jhansen.fokus.controller.FokusController
import javafx.beans.property.SimpleBooleanProperty
import javafx.event.EventTarget
import javafx.scene.layout.Priority
import javafx.scene.text.FontWeight
import javafx.stage.Screen
import org.controlsfx.glyphfont.FontAwesome
import org.controlsfx.glyphfont.Glyph
import org.controlsfx.glyphfont.GlyphFontRegistry
import tornadofx.* // ktlint-disable no-wildcard-imports

class TopLevelView : View("Fokus") {

    companion object {
        private val listHeight = Screen.getPrimary().visualBounds.height
    }

    private val controller: FokusController by di()

    private val fontAwesome = GlyphFontRegistry.font("FontAwesome")
    private val listGlyph = fontAwesome.create(FontAwesome.Glyph.LIST)
    private val focusGlyph = fontAwesome.create(FontAwesome.Glyph.BULLSEYE)
    private val priorityGlyph = fontAwesome.create(FontAwesome.Glyph.SORT_AMOUNT_ASC)
    private val addTaskGlyphFocus = fontAwesome.create(FontAwesome.Glyph.PLUS)
    private val addTaskGlyphList = fontAwesome.create(FontAwesome.Glyph.PLUS)
    private val addPriorityGlyph = fontAwesome.create(FontAwesome.Glyph.PLUS)

    private val showWaitingProperty = SimpleBooleanProperty(false)

    init {
        listGlyph.rotate = 90.0
        focusGlyph.rotate = 90.0
        priorityGlyph.rotate = 270.0

        controller.initFilter(showWaitingProperty.value)
    }

    override val root = drawer {
        item(icon = focusGlyph, expanded = true) {
            fokusDrawer("Focus", "Create new task", addTaskGlyphFocus,
                    addButtonAction = { find<CreateTaskModal>().openModal() },
                    listViewSupplier = { it.listview(controller.focusedTask) {
                        cellFragment(TaskCardFragment::class)
                        prefHeight = listHeight
                    } }
            )
        }
        item(icon = listGlyph) {
            fokusDrawer("List", "Create new task", addTaskGlyphList,
                    addButtonAction = { find<CreateTaskModal>().openModal() },
                    listViewSupplier = { it.listview(controller.taskList) {
                        cellFragment(TaskCardFragment::class)
                        prefHeight = listHeight
                    } }
            )
        }
        item(icon = priorityGlyph) {
            fokusDrawer("Priorities", "Create new priority", addPriorityGlyph,
                    addButtonAction = { find<CreatePriorityModal>().openModal() },
                    listViewSupplier = { it.listview(controller.priorities) {
                        cellFragment(PriorityCardFragment::class)
                        prefHeight = listHeight
                    } }
            )
        }

        minWidth = 800.0
    }

    private fun DrawerItem.fokusDrawer(
        title: String,
        addTooltip: String,
        glyph: Glyph,
        addButtonAction: () -> Unit,
        listViewSupplier: (EventTarget) -> Unit
    ) {
        this.vbox {
            style {
                paddingTop = 5
            }
            hbox {
                label(title) {
                    style {
                        fontWeight = FontWeight.BOLD
                        fontSize = 18.px
                        paddingLeft = 10
                        paddingTop = 5
                        paddingBottom = 5
                    }
                }
                region {
                    hboxConstraints {
                        hGrow = Priority.ALWAYS
                    }
                }
                checkbox("Show waiting", showWaitingProperty) {
                    action {
                        controller.updateFilter(isSelected)
                    }
                    style {
                        paddingRight = 20
                    }
                }
                button(graphic = glyph) {
                    action {
                        addButtonAction.invoke()
                    }
                    tooltip(addTooltip)
                }
                style {
                    paddingRight = 10
                }
            }
            listViewSupplier(this)
        }
    }
}