package io.jhansen.fokus.view

import io.jhansen.fokus.controller.FokusController
import io.jhansen.fokus.model.domain.Task
import io.jhansen.fokus.model.mutable.MutableTask
import javafx.beans.property.ObjectProperty
import javafx.scene.layout.Priority
import javafx.scene.paint.Color
import org.controlsfx.glyphfont.FontAwesome
import org.controlsfx.glyphfont.GlyphFontRegistry
import tornadofx.* // ktlint-disable no-wildcard-imports
import java.time.format.DateTimeFormatter

class TaskCardFragment : ListCellFragment<MutableTask>() {
    private val controller: FokusController by di()
    private val model = TaskCardItemViewModel(itemProperty)

    private val fontAwesome = GlyphFontRegistry.font("FontAwesome")
    private val completeGlyph = fontAwesome.create(FontAwesome.Glyph.CHECK_SQUARE)
    private val editGlyph = fontAwesome.create(FontAwesome.Glyph.EDIT)
    private val deleteGlyph = fontAwesome.create(FontAwesome.Glyph.TRASH)
    private val snoozeGlyph = fontAwesome.create(FontAwesome.Glyph.CLOCK_ALT)
    private val recurGlyph = fontAwesome.create(FontAwesome.Glyph.UNDO)
    private val waitingGlyph = fontAwesome.create(FontAwesome.Glyph.CLOCK_ALT)

    override val root = borderpane {
        top = hbox {
            label(model.summary)
            region {
                hboxConstraints {
                    hGrow = Priority.ALWAYS
                }
            }
            button(graphic = completeGlyph) {
                action {
                    controller.completeTask(model.id.value)
                }
                tooltip("Complete task")
            }
            button(graphic = editGlyph) {
                action {
                    val params = "task" to MutableTask(Task(model))
                    find<EditTaskModal>(params).openModal()
                }
                tooltip("Edit task")
            }
            button(graphic = deleteGlyph) {
                action {
                    controller.deleteTask(model.id.value)
                }
                tooltip("Delete task")
            }
            button(graphic = snoozeGlyph) {
                action {
                    val params = "task" to Task(model)
                    find<SnoozeModal>(params).openModal()
                }
                tooltip("Snooze task")
            }
            style {
                paddingBottom = 10
            }
        }
        bottom = hbox {
            hbox {
                label(model.labels)
                region {
                    hboxConstraints {
                        hGrow = Priority.ALWAYS
                        minWidth = 10.0
                    }
                }
                label(model.waitUntil.select {
                    "Waiting until: ${it.format(DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm"))}".toProperty()
                }) {
                    removeWhen(model.isWaiting.select { (!it).toProperty() })
                }
            }
            region {
                hboxConstraints {
                    hGrow = Priority.ALWAYS
                }
            }
            label(graphic = waitingGlyph) {
                tooltip("This task is waiting")
                removeWhen(model.isWaiting.select { (!it).toProperty() })
            }
            region {
                hboxConstraints {
                    minWidth = 4.0
                }
            }
            label(graphic = recurGlyph) {
                tooltip("This is a recurring task")
                removeWhen(model.recurring.select { (!it).toProperty() })
            }
        }
        style {
            borderColor += box(Color.BLACK)
            padding = box(Dimension(5.toDouble(), Dimension.LinearUnits.px))
        }
    }
}

class TaskCardItemViewModel(property: ObjectProperty<MutableTask>) : ItemViewModel<MutableTask>(itemProperty = property) {
    val id = bind { item?.idProperty }
    val summary = bind { item?.summaryProperty }
    val labels = bind { item?.labelsProperty }
    val waitUntil = bind { item?.waitUntilProperty }
    val isWaiting = bind { item?.isWaitingProperty }
    val completed = bind { item?.completedProperty }
    val recurring = bind { item?.recurringProperty }
    val recurData = bind { item?.recurData }
}
