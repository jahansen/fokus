package io.jhansen.fokus.view

import io.jhansen.fokus.controller.FokusController
import io.jhansen.fokus.model.view.EditTaskViewModel
import io.jhansen.fokus.model.mutable.MutableTask
import io.jhansen.fokus.model.domain.RecurData
import io.jhansen.fokus.model.domain.RecurPeriod
import io.jhansen.fokus.model.domain.Task
import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.ComboBox
import javafx.util.converter.NumberStringConverter
import tornadofx.* // ktlint-disable no-wildcard-imports

class CreateTaskModal : Fragment() {
    private val controller: FokusController by di()
    private val model = EditTaskViewModel(MutableTask(Task(summary = "")))

    private var periodField: ComboBox<String> by singleAssign()

    private val recurPeriods = RecurPeriod.values().map { it.label }
    private val recurPeriodsDefault = SimpleStringProperty(recurPeriods.first())

    override val root = form {
        fieldset("Create a new task") {
            field("Summary") {
                textfield(model.summary).validator {
                    if (it.isNullOrBlank()) error("Summary is required") else null
                }
                maxWidth = 1000.0
            }
            field("Labels") {
                textfield(model.labels)
                maxWidth = 1000.0
            }
            field("Wait until") {
                datepicker(model.waitUntil)
            }

            checkbox(text = "Recur", property = model.recurring)
            form {
                visibleWhen(model.recurring)
                vbox {
                    hbox {
                        label("Repeat every ")
                        textfield(model.interval, NumberStringConverter()) {
                            validator {
                                if (model.recurring.value && it.isNullOrBlank()) error("Interval is required") else null
                            }
                            filterInput { it.controlNewText.isInt().and(it.controlNewText != "0") }
                            maxWidth = 50.0
                        }
                        region {
                            minWidth = 10.0
                        }
                        periodField = combobox(property = recurPeriodsDefault, values = recurPeriods)
                        paddingBottom = 10
                    }
                    hbox {
                        enableWhen(periodField.valueProperty().isEqualTo("weeks"))
                        label("On: ")
                        checkbox("Mo") { paddingRight = 10 }
                        checkbox("Tu") { paddingRight = 10 }
                        checkbox("We") { paddingRight = 10 }
                        checkbox("Th") { paddingRight = 10 }
                        checkbox("Fr") { paddingRight = 10 }
                        checkbox("Sa") { paddingRight = 10 }
                        checkbox("Su") { paddingRight = 10 }
                    }
                }
            }
        }

        hbox {
            paddingBottom = 20

            button("Create") {
                enableWhen(model.valid)
                action {
                    val recurData = if (model.recurring.value) {
                        val recurPeriod = RecurPeriod.fromIndex(periodField.selectionModel.selectedIndex)
                        val weeklyPeriod = recurPeriod == RecurPeriod.WEEKLY
                        RecurData(
                                period = recurPeriod,
                                interval = model.interval.value.toInt(),
                                recurOnMonday = if (weeklyPeriod) model.recurOnMonday.value else false,
                                recurOnTuesday = if (weeklyPeriod) model.recurOnTuesday.value else false,
                                recurOnWednesday = if (weeklyPeriod) model.recurOnWednesday.value else false,
                                recurOnThursday = if (weeklyPeriod) model.recurOnThursday.value else false,
                                recurOnFriday = if (weeklyPeriod) model.recurOnFriday.value else false,
                                recurOnSaturday = if (weeklyPeriod) model.recurOnSaturday.value else false,
                                recurOnSunday = if (weeklyPeriod) model.recurOnSunday.value else false
                        )
                    } else {
                        null
                    }

                    // NOTE: The waituntil field should ideally allow nulls - to be done after DB migrations are
                    // implemented
                    val newTask = Task(
                            summary = model.summary.value,
                            labels = model.labels.value,
                            waitUntil = model.waitUntil.value?.atStartOfDay(),
                            completed = false,
                            recurData = recurData
                    )

                    controller.createTask(newTask)

                    close()
                }
            }
        }
    }
}