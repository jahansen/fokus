package io.jhansen.fokus.extensions

import javafx.beans.property.Property
import javafx.beans.value.ObservableValue
import javafx.event.EventTarget
import tornadofx.* // ktlint-disable no-wildcard-imports
import tornadofx.control.DateTimePicker
import java.time.LocalDateTime

// TornadoFX extensions for DateTimePicker
fun DateTimePicker.bind(property: ObservableValue<LocalDateTime>, readonly: Boolean = false) {
    ViewModel.register(dateTimeValueProperty(), property)
    if (readonly || (property !is Property<*>)) dateTimeValueProperty().bind(property) else dateTimeValueProperty().bindBidirectional(property as Property<LocalDateTime>)
}

fun EventTarget.datetimepicker(op: DateTimePicker.() -> Unit = {}) = opcr(this, DateTimePicker(), op)
fun EventTarget.datetimepicker(property: Property<LocalDateTime>, op: DateTimePicker.() -> Unit = {}) = datetimepicker().apply {
    bind(property)
    op(this)
}