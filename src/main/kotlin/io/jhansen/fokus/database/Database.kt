package io.jhansen.fokus.database

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils.createMissingTablesAndColumns
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import java.sql.Connection

class Database {

    fun initialiseDatabase(dbPath: String) {
        Database.connect("jdbc:sqlite:$dbPath", driver = "org.sqlite.JDBC")
        TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE

        transaction {
            createMissingTablesAndColumns(
                    Tasks,
                    RecurTemplate,
                    Priorities
            )
        }
    }
}

object Tasks : Table() {
    val id = long("id").autoIncrement().primaryKey()
    val summary = varchar("summary", length = 512)
    val labels = varchar("labels", length = 512)
    val wait_until = long("wait_until")
    val completed = bool("completed")
    val recur_id = long("recur_id").references(RecurTemplate.id).nullable()
}

object RecurTemplate : Table() {
    val id = long("id").autoIncrement().primaryKey()
    val period = integer("period")
    val interval = integer("interval")
    val recurOnMonday = bool("recur_on_monday")
    val recurOnTuesday = bool("recur_on_tuesday")
    val recurOnWednesday = bool("recur_on_wednesday")
    val recurOnThursday = bool("recur_on_thursday")
    val recurOnFriday = bool("recur_on_friday")
    val recurOnSaturday = bool("recur_on_saturday")
    val recurOnSunday = bool("recur_on_sunday")
}

object Priorities : Table() {
    val id = long("id").autoIncrement().primaryKey()
    val label = varchar("label", 512)
    val rank = integer("rank")
}