package io.jhansen.fokus

import io.jhansen.fokus.database.Database
import io.jhansen.fokus.view.TopLevelView
import javafx.application.Application
import javafx.scene.image.Image
import javafx.stage.Stage
import tornadofx.* // ktlint-disable no-wildcard-imports
import kotlin.reflect.KClass

class Fokus : App(TopLevelView::class) {
    init {
        addStageIcon(Image("/fokus.png"))

        FX.dicontainer = object : DIContainer {
            val dagger = DaggerDIComponent.builder().build()

            @Suppress("UNCHECKED_CAST")
            override fun <T : Any> getInstance(type: KClass<T>): T = dagger.root() as T
        }
    }

    override fun start(stage: Stage) {
        super.start(stage)
        stage.height = 750.0
    }
}

fun main(args: Array<String>) {

    val dbPath = if (args.size == 1) args[0] else "fokus.db"

    val database = Database()
    database.initialiseDatabase(dbPath)

    Application.launch(Fokus::class.java, *args)
}