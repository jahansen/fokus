package io.jhansen.fokus.util

import java.time.LocalDateTime
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TimeUtil @Inject constructor() {

    fun now(): LocalDateTime {
        return LocalDateTime.now()
    }
}