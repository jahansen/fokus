# Fokus
Fokus is a cross-platform personal organisation desktop application written in 
[Kotlin](https://kotlinlang.org/), using the [TornadoFX](https://tornadofx.io/) framework.

Fokus differs from most other similar programs by clearly separating the act of recording tasks
from completing them. Instead of being a simple 'todo list', the goal of Fokus is to encourage
the user to think about their priorities and the best way to tackle their list of tasks, and 
then present the user with the ideal task to work on at any given time, hiding away other tasks
until the time is right. That is, bringing 'focus' to your work.

Fokus is currently in early development and is lacking most of the features mentioned above,
but does already function as a minimalistic todo list application.

## Developing Fokus

### Dev environment setup
Fokus requires JavaFX and the SQLite JDBC driver to be on the classpath. You can add this in 
IntelliJ IDEA by going to Project Settings > SDKs and adding `jfxrt.jar` and the `sqlite-jdbc` jar 
to the classpath section.

As of Java 8, JavaFX is no longer included by default with Java and you may need to install it
separately.

### Linting
Fokus is integrated with [ktlint](https://ktlint.github.io/). You can run the linter check with `gradle lintKotlin`
and automatically fix errors with `gradle formatKotlin`. 

## Running Fokus
### From the command line
Fokus is built as a 'fat jar' containing all its dependencies. However, the SQLite JDBC driver
still needs to be included in the classpath at runtime. As such, Fokus can be run on the command
line with this following command:

```
java -cp /path/to/sqlite-jdbc.jar io.jhansen.fokus.FokusKt /path/to/database.db
```

### From IntelliJ IDEA
Fokus can be run from IntelliJ IDEA simply by right-clicking within the class `Fokus.kt` and 
selecting 'Run'.  

## Releasing Fokus
Included in the repo is a simple script for building Fokus and copying the result to the `.fokus`
folder in the user's (Linux/MacOS) home directory. This is essentially intended for development and 
dogfooding purposes and isn't a proper release process (which will be added eventually...).

